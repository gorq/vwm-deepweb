import database.Database;
import database.HiddenDatabaseInterface;
import database.utils.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void getAllCountries(Database d) {
        List<String> countries = d.getValidOrigins();
        List<String> types = d.getValidComputerTypes();

        for (String country : countries) {
            for (String type : types) {
                System.out.printf("Checking country: %s type: %s\n", country, type);
                Query q = new Query(type).addWantedOrigin(country);
                ResultSet r = d.executeQuery(q);
                d.printResultSet(r);
            }
        }
    }

    public static void getAllTypes(Database d) {
        List<String> types = d.getValidComputerTypes();
        for(String type : types) {
            System.out.println("Checking type: " + type);
            Query q = new Query(type);
            ResultSet r = d.executeQuery(q);
            d.printResultSet(r);
        }
    }

    // a fixed ordering of attributes is used here
    public static Set<String> getRandomTuples(Database d) throws SQLException {
        Random random = new Random();
        Set<String> result = new TreeSet<>();

        List<String> types = d.getValidComputerTypes();
        List<String> countries = d.getValidOrigins();
        List<String> brands = d.getValidBrands();
        List<String> statuses = d.getValidStatuses();

        String type = types.get(Math.abs(random.nextInt() % types.size()));
        String country = "";
        String brand = "";
        String status = "";

        // get random sample via random walk
        Query q = new Query(type);
        if (d.getQuerySize(q) <= d.getResultLimit()) {
            ResultSet res = d.executeQuery(q);
            System.out.printf("Type: %s, Brand: %s, Origin: %s, Status: %s\n", type, brand, country, status);
            d.printResultSet(res);
            while (res.next()) {
                result.add(res.getString("ComputerID"));
            }
            return result;
        }

        country = countries.get(Math.abs(random.nextInt() % countries.size()));
        q.addWantedOrigin(country);
        if (d.getQuerySize(q) <= d.getResultLimit()) {
            ResultSet res = d.executeQuery(q);
            System.out.printf("Type: %s, Brand: %s, Origin: %s, Status: %s\n", type, brand, country, status);
            d.printResultSet(res);
            while (res.next())
                result.add(res.getString("ComputerID"));
            return result;
        }

        brand = brands.get(Math.abs(random.nextInt() % brands.size()));
        q.addWantedBrand(brand);
        if (d.getQuerySize(q) <= d.getResultLimit()) {
            ResultSet res = d.executeQuery(q);
            System.out.printf("Type: %s, Brand: %s, Origin: %s, Status: %s\n", type, brand, country, status);
            d.printResultSet(res);
            while (res.next())
                result.add(res.getString("ComputerID"));
            return result;
        }

        status = statuses.get(Math.abs(random.nextInt() % statuses.size()));
        q.setStatus(status);
        if (d.getQuerySize(q) <= d.getResultLimit()) {
            ResultSet res = d.executeQuery(q);
            System.out.printf("Type: %s, Brand: %s, Origin: %s, Status: %s\n", type, brand, country, status);
            d.printResultSet(res);
            while (res.next())
                result.add(res.getString("ComputerID"));
            return result;
        }

        return result;
    }

    public static void testInterface() {
        // database name to be set
        String dbName = "DeepWebDatabase";
        // driver + host + port setup
        String dbConnection = "jdbc:mysql://localhost:3306/";
        // connection flags
        String dbFlags = "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
        // user to log in as
        String user = "gorq";
        // password for the user
        String password = "Mamina11";
        // create the database
        Database d = new HiddenDatabaseInterface(dbConnection+dbName+"?"+dbFlags, user, password);

        getAllTypes(d);

        Set<String> total = new TreeSet<>();
        try {
            Set<String> s = getRandomTuples(d);
            total.addAll(s);
            System.out.println("Size of s: " + s.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Total number of valid results: " + total.size());
    }

    public static void main(String[] args) {
        testInterface();
    }
}
