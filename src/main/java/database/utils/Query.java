package database.utils;

import java.util.List;
import java.util.ArrayList;

/**
 * A class representing a query into the hidden database
 */
public class Query {
    /**
     * Simple constructor that sets the attributes to default values
     * type has to be specified, as it represents the radio button
     * option of the interface
     * @param type is the type of computers to be looked for
     */
    public Query(String type) {
        m_WantedBrands = new ArrayList<>();
        m_WantedOrigins = new ArrayList<>();
        m_Type = type;
        m_Model = null;
        m_ScreenSize = null;
        m_Status = null;
        m_LowPrice = null;
        m_HighPrice = null;
    }

    /**
     * Add wanted origins of the computers
     * @param id is the id of the origin country
     * @return this
     */
    public Query addWantedOrigin(String id) {
        m_WantedOrigins.add(id);
        return this;
    }

    /**
     * Add wanted brands of the computers
     * @param id is the id of the computers
     * @return this
     */
    public Query addWantedBrand(String id) {
        m_WantedBrands.add(id);
        return this;
    }

    /**
     * Set model of the computers to be matched againts
     * @param model is the model
     * @return this
     */
    public Query setModel(String model) {
        m_Model = model;
        return this;
    }

    /**
     * Set the price range for the computers
     * @param lo is the lower bound
     * @param hi is the upper bound
     * @return this
     */
    public Query priceBetween(int lo, int hi) {
        assert(lo < hi);
        m_LowPrice = lo;
        m_HighPrice = hi;
        return this;
    }

    /**
     * Set status of the computers
     * @param status is the wanted status
     * @return this
     */
    public Query setStatus(String status) {
        m_Status = status;
        return this;
    }

    /**
     * Set the screen size for the computers
     * @param value is the value
     * @return this
     */
    public Query setScreenSize(String value) {
        m_ScreenSize = value;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sql = new StringBuilder();
        sql.append(" where Computer.Type = ").append(m_Type);

        // add models to query
        if (m_WantedBrands.size() != 0) {
            sql.append(" and Computer.BrandID in (");
            for(int i = 0; i < m_WantedBrands.size(); ++i) {
                if (i != 0)
                    sql.append(",");
                sql.append(m_WantedBrands.get(i));
            }
            sql.append(") ");
        }

        // add valid origins
        if (m_WantedOrigins.size() != 0) {
            sql.append(" and Computer.MadeIn in (");
            for(int i = 0; i < m_WantedOrigins.size(); ++i) {
                if (i != 0)
                    sql.append(",");
                sql.append(m_WantedOrigins.get(i));
            }
            sql.append(") ");
        }

        if (m_LowPrice != null)
            sql.append(" and Computer.Price between ").append(m_LowPrice).append(" and ").append(m_HighPrice);

        if (m_Status != null)
            sql.append(" and Computer.Status = ").append(m_Status);

        if (m_Model != null)
            sql.append(" and Computer.Model like '%").append(m_Model).append("%'");

        if (m_ScreenSize != null)
            sql.append(" and Computer.ScreenSize = ").append(m_ScreenSize);

        return sql.toString();
    }

    private List<String> m_WantedBrands;
    private List<String> m_WantedOrigins;
    private String       m_Type;
    private String       m_Model;
    private String       m_ScreenSize;
    private String       m_Status;
    private Integer      m_LowPrice;
    private Integer      m_HighPrice;
}
