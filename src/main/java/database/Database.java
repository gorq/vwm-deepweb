package database;

import database.utils.Query;

import java.sql.ResultSet;
import java.util.List;

/**
 * Database interface representing our database
 */
public interface Database {
    /**
     * Get all the valid brand ID's a user can choose from
     * @return list of id's
     */
    List<String> getValidBrands();

    /**
     * Get all the valid country of origin ID's a user can choose from
     * @return list of id's
     */
    List<String> getValidOrigins();


    /**
     * Get all the valid computer type ID's a user can choose from
     * @return list of id's
     */
    List<String> getValidComputerTypes();


    /**
     * Get all the valid status ID's a user can choose from
     * @return list of id's
     */
    List<String> getValidStatuses();


    /**
     * Execute a given query, this method already joins the tables so that we have a proper result
     * @param q is the query to be executed
     * @return result set returned by the database
     */
    ResultSet executeQuery(Query q);


    /**
     * Print a result set of computers
     * @param r is the result set to be printed
     */
    void printResultSet(ResultSet r);


    /**
     * Limit of results per query
     * @return the result limit
     */
    int getResultLimit();

    /**
     * Get the size of the query for the user
     * @param q is the query to be executed
     * @return the result size of the query
     */
    int getQuerySize(Query q);
}
