package database;

import database.utils.Query;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A class representing the interface available to us as a user
 */
public class HiddenDatabaseInterface implements Database {
    public HiddenDatabaseInterface(String url, String userName, String password) {
        establishConnection(url,userName, password);
        m_Limit = 10; // this is the limit of the result given by our database
        /*
        m_MainQuery =
                "select ComputerID," +
                        " Brand.Name as Brand," +
                        " Country.Name as Country," +
                        " Model," +
                        " Price," +
                        " ScreenSize," +
                        " ComputerType.Value as Type," +
                        " Status.Value as Status" +
                        " from Computer join Brand join Country join ComputerType join Status" +
                        " where Computer.BrandID = Brand.BrandID" +
                        " and   Computer.MadeIn = Country.CountryID" +
                        " and   Computer.Type = ComputerType.TypeID" +
                        " and   Computer.Status = Status.StatusID";
        */
        m_MainQuery = "select ComputerID, Model from Computer";
    }

    /**
     * Establish a connection with a given database
     * @param url is the url settings to connect to
     * @param userName is the username to use
     * @param password is the password to use
     */
    private void establishConnection(String url, String userName, String password) {
        try {
            m_Connection = DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            System.out.println("Could not establish connection.");
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValidBrands() {
        String query = getQuery(0, "Brand", "BrandID");
        return makeListFromResult(executeQuery(query), "BrandID");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValidOrigins() {
        String query = getQuery(0, "Country", "CountryID");
        return makeListFromResult(executeQuery(query), "CountryID");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValidComputerTypes(){
        String query = getQuery(0, "ComputerType", "TypeID");
        return makeListFromResult(executeQuery(query), "TypeID");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getValidStatuses() {
        String query = getQuery(0, "Status", "StatusID");
        return makeListFromResult(executeQuery(query), "StatusID");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResultSet executeQuery(Query q) {
        return executeQuery(m_MainQuery + q.toString() + " limit " + m_Limit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void printResultSet(ResultSet r) {
        String[] columns = {"ComputerID", "Model"};


        for(String s : columns)
            System.out.printf("%20s", s);
        System.out.println();
        System.out.println();
        if (r != null) {
            try {
                while (r.next()) {
                    for(String s : columns)
                        System.out.printf("%20s", r.getString(s));
                    System.out.println();
                }
                // return pointer to before the first row
                r.beforeFirst();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getResultLimit() {
        return m_Limit;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getQuerySize(Query q) {
        ResultSet rs = executeQuery(m_MainQuery + q.toString());
        // calculate size
        int size = 0;
        try {
            if (rs != null)
            {
                rs.beforeFirst();
                rs.last();
                size = rs.getRow();
            }
        } catch (SQLException e) {
            System.out.println("Error while trying to get query size.");
            e.printStackTrace();
        }

        return size;
    }

    /**
     * Execute a given query
     * @param query is the query to execute
     * @return result of such a query
     */
    private ResultSet executeQuery(String query){
        try {
            Statement t = m_Connection.createStatement();
            return t.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Make list of results from a given set with specified column
     * @param r is the result set to make list from
     * @param column is the column to get
     * @return list of values
     */
    private List<String> makeListFromResult(ResultSet r, String column) {
        List<String> l = new ArrayList<String>();
        if (r == null)
            return l;
        try {
            while (r.next()) {
                l.add(r.getString(column));
            }
        } catch (SQLException e) {
            System.out.println("Error while processing a result from query.");
            e.printStackTrace();
        }

        return l;
    }

    /**
     * Create a simple select query
     * @param limit is the limit of the result
     * @param table is the table to get result from
     * @param columns is the columns we are getting
     * @return sql string representing given statement
     */
    private String getQuery(int limit, String table, String ...columns) {
        StringBuilder sql = new StringBuilder("select ");
        for(String s : columns)
            sql.append(s).append(",");

        // remove last ','
        sql = new StringBuilder(sql.substring(0, sql.length() - 1));

        sql.append(" from ").append(table);
        if (limit != 0)
            sql.append("limit ").append(limit);
        return sql.toString();
    }

    private Connection m_Connection;
    private String     m_MainQuery;
    private int        m_Limit;
}
